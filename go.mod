module gitlab.com/vorticist/killer-koala

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.4.1 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/stretchr/testify v1.6.0 // indirect
	github.com/tidwall/pretty v1.0.1 // indirect
	github.com/valyala/fasttemplate v1.1.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/vorticist/logger v0.0.0-20200604030304-8a8d6d4d3bb3
	go.mongodb.org/mongo-driver v1.1.2
	golang.org/x/crypto v0.0.0-20200602180216-279210d13fed // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
)
